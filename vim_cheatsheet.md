# Vim Cheatsheet

## Navigating to files

You use telescope for moving and can search for files with `<space>-sf` (space search file).
Use `<C>n` or `<C>p` to go up or down in the list.
Once a file have been found that should be opened, use:

* `Enter` to open in current buffer.
* `<C>-v` to open in vertical split.
* `<C>-x` to open in horizontal split.

## Changes

### Diff

* `:w !diff % -` to diff the current file with the saved version

## Moving

You should think of the directions like you've taken the regular arrow layout and splatted the up down on the row of the other three keys, placing it to the right of the down key.

| Direction | key |
| --------- | --- |
| Left      | h   |
| Right     | l   |
| Up        | k   |
| Down      | j   |
| Page down | `<C-f>` |
| Page up   | `<C-b>` |
| Half page down | `<C-d>` |
| Half page up | `<C-u>` |
| Move to window | `<C-w>` + h/j/k/l |
| Move to window alternative | `<C>` + h/j/k/l |
| Move to window which works with tmux | <C-h/j/k/l> |

### LSP related

When you have jumped into some definition with `gd` or `gD` you can go back with `<C-o>` or multijump can be handled with `<C-i>`

## Common text editing functions

### Copy element

if you want to copy an element in the middle of things `y%`.

### Move selection

If you want to move a selection you need to:

1. select the lines to move with [ctrl|shift] + v
2. use + for down and - for up and then the amount of lines, fx 4
3. call `:move -/+Amount` fx `:move +4` to move it 4 lines down

### Replace pattern

vim has more or less the same pattern as using sed, however you will probably want to prefix the command with `%s` rather than `s`.
This is because you need to search the whole file rather than just this line.
Typically you want:

```vim
:%s/replace/with/g
```

if you want it to ask you before each replace you can add `c` to the flags, fx:

```vim
:%s/replace/with/gc
```

## Surround text

You can find the supported commands [at the surround plugins doc](https://github.com/tpope/vim-surround).

### Surround word

`ys` starts sourround
`iw` selects InnerWord

then press the character that needs to sourround the word, fx ".

## File functions

### Create file

`:action NewFile`

### Create directory

`:action NewDir`

## Tabs

### New

`tabe filename`

### Next

- `:tabn`
- `gt`

### Previous

- `:tabp`
- `gT`

## Comments

### Line

`gc`

### Block

`gb`
