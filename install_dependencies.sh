#!/usr/bin/env bash

set -euo pipefail

nvim_dir=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
ask_become_pass_option=""

if [ -z "${ANSIBLE_BECOME_PASS:-}" ]; then
    ask_become_pass_option="--ask-become-pass"
fi

cd "$nvim_dir/dependencies"
python -m pipenv sync
python -m pipenv run ansible-playbook $ask_become_pass_option setup.yml
