return {
  'nvim-neotest/neotest',
  dependencies = {
    'nvim-neotest/nvim-nio',
    'nvim-neotest/neotest-python',
    'nvim-lua/plenary.nvim',
    'antoinemadec/FixCursorHold.nvim',
    'nvim-treesitter/nvim-treesitter',
    'folke/lazydev.nvim',
    {
      'fredrikaverpil/neotest-golang',
      dependencies = {
        'leoluz/nvim-dap-go',
      },
    },
  },
  config = function()
    local neotest_ns = vim.api.nvim_create_namespace 'neotest'
    vim.diagnostic.config({
      virtual_text = {
        format = function(diagnostic)
          local message = diagnostic.message:gsub('\n', ' '):gsub('\t', ' '):gsub('%s+', ' '):gsub('^%s+', '')
          return message
        end,
      },
    }, neotest_ns)
    require('neotest').setup {
      adapters = {
        require 'neotest-golang',
        require 'neotest-python',
      },
      log_level = 'debug',
    }
  end,
  vim.keymap.set(
    'n',
    '<leader>td',
    ':lua require("neotest").run.run({strategy = "dap"})<CR>',
    { noremap = true, desc = '[t]est [d]ebug - debugs the nearest test' }
  ),
  vim.keymap.set('n', '<leader>tr', ':lua require("neotest").run.run()<CR>', { noremap = true, desc = '[t]est [r]un - runs the nearest test' }),
  vim.keymap.set(
    'n',
    '<leader>tf',
    ':lua require("neotest").run.run(vim.fn.expand("%"))<CR>',
    { noremap = true, desc = '[t]est [f]ile - run all tests in the current file' }
  ),
  vim.keymap.set('n', '<leader>tl', ':lua require("neotest").run.run_last()<CR>', { noremap = true, desc = '[t]est [l]ast - reruns the last test' }),
}
