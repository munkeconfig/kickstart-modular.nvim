return {
  'christoomey/vim-tmux-navigator',
  cmd = {
    'TmuxNavigateLeft',
    'TmuxNavigateDown',
    'TmuxNavigateUp',
    'TmuxNavigateRight',
    'TmuxNavigatePrevious',
    'TmuxNavigatorProcessList',
  },
  keys = {
    { '<c-h>', '<cmd><C-U>TmuxNavigateLeft<cr>', { desc = 'Move focus to the left window' } },
    { '<c-j>', '<cmd><C-U>TmuxNavigateDown<cr>', { desc = 'Move focus to the bottom window' } },
    { '<c-k>', '<cmd><C-U>TmuxNavigateUp<cr>', { desc = 'Move focus to the top window' } },
    { '<c-l>', '<cmd><C-U>TmuxNavigateRight<cr>', { desc = 'Move focus to the right window' } },
    { '<c-\\>', '<cmd><C-U>TmuxNavigatePrevious<cr>', { desc = 'Move focus to the previous window' } },
  },
}
