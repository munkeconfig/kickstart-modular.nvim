-- [[ Basic Keymaps ]]
--  See `:help vim.keymap.set()`

-- Clear highlights on search when pressing <Esc> in normal mode
--  See `:help hlsearch`
vim.keymap.set('n', '<Esc>', '<cmd>nohlsearch<CR>')

-- Diagnostic keymaps
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, { desc = 'Open diagnostic [Q]uickfix list' })

-- Exit terminal mode in the builtin terminal with a shortcut that is a bit easier
-- for people to discover. Otherwise, you normally need to press <C-\><C-n>, which
-- is not what someone will guess without a bit more experience.
--
-- NOTE: This won't work in all terminal emulators/tmux/etc. Try your own mapping
-- or just use <C-\><C-n> to exit terminal mode
vim.keymap.set('t', '<Esc><Esc>', '<C-\\><C-n>', { desc = 'Exit terminal mode' })

-- TIP: Disable arrow keys in normal mode
-- vim.keymap.set('n', '<left>', '<cmd>echo "Use h to move!!"<CR>')
-- vim.keymap.set('n', '<right>', '<cmd>echo "Use l to move!!"<CR>')
-- vim.keymap.set('n', '<up>', '<cmd>echo "Use k to move!!"<CR>')
-- vim.keymap.set('n', '<down>', '<cmd>echo "Use j to move!!"<CR>')

-- Keybinds to make split navigation easier.
--  Use CTRL+<hjkl> to switch between windows
--
--  See `:help wincmd` for a list of all window commands
-- vim.keymap.set('n', '<C-h>', '<cmd> TmuxNavigateLeft<CR>', { desc = 'Move focus to the left window' })
-- vim.keymap.set('n', '<C-l>', '<cmd> TmuxNavigateRight<CR>', { desc = 'Move focus to the right window' })
-- vim.keymap.set('n', '<C-j>', '<cmd> TmuxNavigateDown<CR>', { desc = 'Move focus to the lower window' })
-- vim.keymap.set('n', '<C-k>', '<cmd> TmuxNavigateUp<CR>', { desc = 'Move focus to the upper window' })

-- [[ Basic Autocommands ]]
--  See `:help lua-guide-autocommands`

-- Highlight when yanking (copying) text
--  Try it with `yap` in normal mode
--  See `:help vim.highlight.on_yank()`
vim.api.nvim_create_autocmd('TextYankPost', {
  desc = 'Highlight when yanking (copying) text',
  group = vim.api.nvim_create_augroup('kickstart-highlight-yank', { clear = true }),
  callback = function()
    vim.highlight.on_yank()
  end,
})

-- Function to get available kubernetes contexts
local function get_kube_contexts()
  local handle = io.popen 'kubectl config get-contexts -o name'
  if not handle then
    return {}
  end

  local contexts = {}
  for context in handle:lines() do
    table.insert(contexts, context)
  end
  handle:close()
  return contexts
end

-- Function to show context picker and apply kubectl command
local function kubectl_apply_with_context()
  local contexts = get_kube_contexts()
  if #contexts == 0 then
    vim.notify('No kubernetes contexts found', vim.log.levels.ERROR)
    return
  end

  vim.ui.select(contexts, {
    prompt = 'Select kubernetes context to apply in:',
    format_item = function(item)
      return item
    end,
  }, function(context)
    if context then
      local cmd = string.format('kubectl --context %s apply -f %s', context, vim.fn.expand '%')
      vim.notify('Executing: ' .. cmd, vim.log.levels.INFO)
      -- vim.cmd('!' .. cmd)
      -- Using terminal buffer instead
      vim.cmd 'new' -- Open new window
      vim.cmd('terminal ' .. cmd) -- Run in terminal buffer
      vim.cmd 'startinsert' -- Enter insert mode
    end
  end)
end

-- Map the function to a key
vim.keymap.set('n', '<leader>ka', kubectl_apply_with_context, { desc = 'kubectl apply -f <the current file> in context you select' })
-- vim: ts=2 sts=2 sw=2 et
